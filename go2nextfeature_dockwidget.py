# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Go2NextFeatureDockWidget
                                 A QGIS plugin
 Allows jumping from a feature to another following an attribute order
                             -------------------
        begin                : 2016-12-27
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Alberto De Luca for Tabacco Editrice
        email                : info@tabaccoeditrice.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os

from collections import OrderedDict
from geo_utils import utils as geo_utils, vector_utils as vutils
from qgis.core import *
from PyQt4 import QtGui, uic
from PyQt4.QtCore import *
from PyQt4.QtGui import *

import buttons_utils, operator

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'go2nextfeature_dockwidget.ui'))


class Go2NextFeatureDockWidget(QtGui.QDockWidget, FORM_CLASS):

    closingPlugin = pyqtSignal()
    plugin_name = 'Go2NextFeature 1.11'

    def __init__(self, iface, parent=None):
        """Constructor."""
        super(Go2NextFeatureDockWidget, self).__init__(parent)
        self.setupUi(self)

        self.iface = iface

        self.feats_od = OrderedDict()
        self.lay_id = None
        self.lay = None
        self.attribs_od = OrderedDict()
        self.attrib = None
        self.ft_pos = -1

        self.tool = None

        self.fra_main.setLineWidth(3)
        fra_main_lay = QVBoxLayout(self.fra_main)
        fra_main_lay.setContentsMargins(0, 0, 0, 0)
        fra_main_lay.setSpacing(0)

        # frame 1
        self.fra_1 = QFrame(self.fra_main)

        fra_1_lay = QFormLayout(self.fra_1)

        self.lbl_layer = QLabel('Layer:')
        self.cbo_layer = QComboBox()
        fra_1_lay.addRow(self.lbl_layer, self.cbo_layer)

        self.lbl_attribute = QLabel('Attribute:')
        self.cbo_attribute = QComboBox()
        fra_1_lay.addRow(self.lbl_attribute, self.cbo_attribute)

        self.lbl_curr = QLabel('Value:')
        self.txt_curr = QLineEdit()
        self.txt_curr.setReadOnly(True)
        fra_1_lay.addRow(self.lbl_curr, self.txt_curr)

        self.lbl_action = QLabel('Action')
        self.fra_action = QFrame(self.fra_1)
        fra_action_lay = QHBoxLayout(self.fra_action)
        fra_action_lay.setContentsMargins(0, 0, 0, 0)
        self.rad_action_pan = QRadioButton('Pan')
        self.rad_action_pan.setChecked(True)
        self.rad_action_zoom = QRadioButton('Zoom')

        self.gra_action = QButtonGroup(self.fra_1)
        self.gra_action.addButton(self.rad_action_pan)
        self.gra_action.addButton(self.rad_action_zoom)

        fra_action_lay.addWidget(self.rad_action_pan)
        fra_action_lay.addWidget(self.rad_action_zoom)
        fra_1_lay.addRow(self.lbl_action, self.fra_action)

        # frame 2
        self.fra_2 = QFrame(self.fra_main)
        fra_2_lay = QHBoxLayout(self.fra_2)
        fra_2_lay.setContentsMargins(0, 0, 0, 0)

        self.chk_start_sel = QCheckBox('Start from selected feature')
        fra_2_lay.addWidget(self.chk_start_sel)

        # frame 3
        self.fra_3 = QFrame(self.fra_main)
        fra_3_lay = QHBoxLayout(self.fra_3)
        fra_3_lay.setContentsMargins(0, 0, 0, 0)

        self.chk_select = QCheckBox('Select')
        self.btn_prev = QPushButton('<')
        self.btn_next = QPushButton('>')

        fra_3_lay.addWidget(self.chk_select)
        fra_3_lay.addWidget(self.btn_prev)
        fra_3_lay.addWidget(self.btn_next)

        # Add everything to main frame
        fra_main_lay.addWidget(self.fra_1)
        fra_main_lay.addWidget(self.fra_2)
        fra_main_lay.addWidget(self.fra_3)
        fra_main_lay.addSpacerItem(QSpacerItem(0, 10, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.setup()

    def setup(self):
        self.setWindowTitle(Go2NextFeatureDockWidget.plugin_name)
        self.chk_select.setEnabled(False)

        QgsMapLayerRegistry.instance().layerWasAdded.connect(self.update_layers_combos)
        QgsMapLayerRegistry.instance().layerRemoved.connect(self.update_layers_combos)

        # go to next
        self.cbo_layer.activated.connect(self.cbo_layer_activated)
        self.cbo_attribute.activated.connect(self.cbo_attrib_activated)

        self.btn_prev.pressed.connect(self.btn_prev_pressed)
        self.btn_next.pressed.connect(self.btn_next_pressed)

        self.update_layers_combos()

        shortcut = QShortcut(QKeySequence(Qt.Key_F8), self.iface.mainWindow())
        shortcut.setContext(Qt.ApplicationShortcut)
        shortcut.activated.connect(self.btn_next_pressed)

    def closeEvent(self, event):
        self.closingPlugin.emit()
        event.accept()

    def update_layers_combos(self):

            prev_lay_id = None
            if self.cbo_layer.currentIndex() > -1:
                prev_lay_id = self.cbo_layer.itemData(self.cbo_layer.currentIndex())

            # Find layers IDs and names
            layer_ids = QgsMapLayerRegistry.instance().mapLayers()
            names_ids_d = {}
            for layer_id in layer_ids:
                layer = geo_utils.LayerUtils.get_lay_from_id(layer_id)
                if layer is not None and layer.type() == 0:
                    names_ids_d[layer_id] = layer.name()

            # Fill combo
            self.cbo_layer.clear()
            self.cbo_layer.addItem('', None)

            sorted_ids_names = sorted(names_ids_d.items(), key=operator.itemgetter(1))

            for name_id in sorted_ids_names:
                self.cbo_layer.addItem(name_id[1], name_id[0])

            if prev_lay_id is not None:
                self.lay_id = self.set_layercombo_index(self.cbo_layer, prev_lay_id)

            self.chk_select.setEnabled(self.lay_id is not None)
            self.btn_next.setEnabled(self.lay_id is not None)
            self.cbo_layer_activated()

            if not self.lay_id:
                self.txt_curr.setText('-')

    def cbo_layer_activated(self):

        self.lay_id = self.cbo_layer.itemData(self.cbo_layer.currentIndex())

        if self.lay_id is not None:

            self.lay = geo_utils.LayerUtils.get_lay_from_id(self.lay_id)
            fields = self.lay.pendingFields()
            self.cbo_attribute.clear()
            for field in fields:

                self.attribs_od[field.name()] = field
                self.cbo_attribute.addItem(field.name(), field)

            self.cbo_attrib_activated()

        else:

            self.lay = None
            self.feats_od.clear()
            self.attrib = None
            self.attribs_od.clear()

    def cbo_attrib_activated(self):

        self.attrib = self.cbo_attribute.itemData(self.cbo_attribute.currentIndex())
        if self.attrib is None:
            return

        self.feats_od.clear()

        feats_d = {}
        for feat in self.lay.getFeatures():
            feats_d[feat.attribute(self.attrib.name())] = feat

        feats_od = OrderedDict(sorted(feats_d.items()))

        self.ft_pos = -1

        self.feats_od.clear()
        pos = 0
        for feat in feats_od.itervalues():
            self.feats_od[pos] = feat
            pos += 1

        self.chk_select.setEnabled(True)
        self.btn_next.setEnabled(True)
        self.btn_prev.setEnabled(False)

        self.txt_curr.setText('-')

    def btn_prev_pressed(self):

        self.ft_pos -= 1
        self.next_ft()

        self.btn_next.setEnabled(True)
        if self.ft_pos == 0:
            self.btn_prev.setEnabled(False)

    def btn_next_pressed(self):

        self.ft_pos += 1
        self.next_ft()

        if self.ft_pos == len(self.feats_od) - 1:
            self.btn_next.setEnabled(False)

        if len(self.feats_od) > 1 and self.ft_pos > 0:
            self.btn_prev.setEnabled(True)

    def next_ft(self):
        if self.chk_start_sel.isChecked():

            sel_ft_ids = self.lay.selectedFeaturesIds()
            if len(sel_ft_ids) != 1:
                self.iface.messageBar().pushWarning(
                    Go2NextFeatureDockWidget.plugin_name,
                    'Please select only one feature.')  # TODO: softcode
                return

            sel_ft_id = sel_ft_ids[0]
            sel_ft_index = -1
            for index, feat in self.feats_od.iteritems():
                if feat.id() == sel_ft_id:
                    sel_ft_index = index
                    break

            if sel_ft_index != -1:
                self.ft_pos = sel_ft_index
                self.chk_start_sel.setChecked(False)

        if 0 <= self.ft_pos < len(self.feats_od):

            renderer = self.iface.mapCanvas().mapRenderer()

            if self.rad_action_pan.isChecked():
                self.iface.mapCanvas().setCenter(renderer.layerToMapCoordinates(
                    self.lay,
                    self.feats_od[self.ft_pos].geometry().centroid().asPoint()))
            elif self.rad_action_zoom.isChecked():

                print renderer.layerToMapCoordinates(
                    self.lay,
                    self.feats_od[self.ft_pos].geometry().boundingBox())

                self.iface.mapCanvas().setExtent(renderer.layerToMapCoordinates(
                    self.lay,
                    self.feats_od[self.ft_pos].geometry().boundingBox()))

            if self.chk_select.isChecked():

                if QGis.QGIS_VERSION_INT >= 21600:
                    self.lay.selectByIds([self.feats_od[self.ft_pos].id()])
                else:
                    self.lay.setSelectedFeatures([self.feats_od[self.ft_pos].id()])

            self.iface.mapCanvas().refresh()

            attrib_value = self.feats_od[self.ft_pos].attribute(self.attrib.name())

            self.txt_curr.setText(str(attrib_value))
            # self.txt_curr.setText(str(self.feats_od[self.ft_pos].id()))

            if self.ft_pos == len(self.feats_od) - 1:
                self.btn_next.setEnabled(False)

            if len(self.feats_od) > 1 and self.ft_pos > 0:
                self.btn_prev.setEnabled(True)

    def set_layercombo_index(self, combo, layer_id):

        index = combo.findData(layer_id)
        if index >= 0:
            combo.setCurrentIndex(index)
            return geo_utils.LayerUtils.get_lay_from_id(self.get_combo_current_data(combo))
        else:
            if combo.count() > 0:
                combo.setCurrentIndex(0)
                return combo.itemData(0)
            else:
                return None

    def get_combo_current_data(self, combo):

        index = combo.currentIndex()
        return combo.itemData(index)

    def set_cursor(self, cursor_shape):
        cursor = QCursor()
        cursor.setShape(cursor_shape)
        self.iface.mapCanvas().setCursor(cursor)


# This file contains metadata for your plugin. Since 
# version 2.0 of QGIS this is the proper way to supply 
# information about a plugin. The old method of 
# embedding metadata in __init__.py will 
# is no longer supported since version 2.0.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Go2NextFeature
qgisMinimumVersion=2.14
description=Allows jumping from a feature to another following an attribute order
version=1.11
author=Alberto De Luca for Tabacco Editrice
email=sergio.gollino@tabaccoeditrice.com

about=This plugin allows to select a layer and jump from a feature to another. It's possible to select an attribute of the the layer (usually an ID) and follow it. Press "F8" to go to the next feature. It's possibile to use a PAN function or a Zoom function(usefull for poligons and lines). The plugin was created for checking the position of every single feature of a layer without using the attribute table.

tracker=https://gitlab.com/albertodeluca/go2nextfeature/issues
repository=https://gitlab.com/albertodeluca/go2nextfeature
# End of mandatory metadata

# Recommended items:

# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=

homepage=
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

